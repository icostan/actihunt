# frozen_string_literal: true

require 'rails_helper'

describe Providers::Runkeeper do
  let(:activity) { FactoryGirl.create :activity }

  before do
    activity.points << FactoryGirl.build(:point)
    activity.points << FactoryGirl.build(:point)
  end

  it '#path', :vcr do
    runkeeper = Providers::Runkeeper.new ''
    path = runkeeper.path activity
    expect(path.size).to eq 2
    point = path.first
    expect(point[:latitude]).to be > 47
    expect(point[:longitude]).to be > 27
    expect(point[:altitude]).to be > 0.0
    expect(point[:timestamp]).to be >= 0
    expect(point[:type]).to eq 'gps'
  end

  it 'maps Rowing to Paddling' do
    runkeeper = Providers::Runkeeper.new ''
    type = runkeeper.from_type 'type' => 'Rowing'
    expect(type).to eq 'Paddling'
  end
end
