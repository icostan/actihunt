# frozen_string_literal: true

require 'rails_helper'

describe Providers::Strava do
  describe '#pull_activity' do
    subject { Providers::Strava.new '' }

    it 'activity with points' do
      a = { id: 'a', type: 'Run', start_date: DateTime.parse('2015-08-04 13:18:00'), utc_offset: 7200, elapsed_time: '300', distance: '1234' }.stringify_keys
      spoint = [0, [47.47, 27.27], 0.0, 69.2]
      epoint = [1919, [47.47, 27.27], 4912.4, 155]
      expect(Strava::Activity).to receive(:points).and_return [spoint, epoint]
      activity = subject.pull_activity a

      expect(activity.uuid).to eq 'a'
      expect(activity.type).to eq 'Running'
      expect(activity.duration).to eq 300
      expect(activity.start_time).to eq DateTime.new(2015, 8, 4, 13, 18, 0o0)
      expect(activity.utc_offset).to eq 2
      expect(activity.points.size).to eq 2

      point = activity.points.first
      expect(point.type).to eq Point::TYPES.first
      expect(point.lat).to eq 47.47
      expect(point.lng).to eq 27.27
      expect(point.distance).to eq 0
      expect(point.altitude).to eq 69.2
    end

    it 'maps EBikeRide to Spinning' do
      type = subject.from_type 'EBikeRide'
      expect(type).to eq 'Spinning'
    end
  end
end
