require 'rails_helper'

RSpec.describe Jobs, type: :model do
  describe Activity do
    let(:user) { FactoryGirl.create :user }
    let!(:r_identity) { FactoryGirl.create :identity, provider: 'runkeeper', user: user }
    let!(:s_identity) { FactoryGirl.create :identity, provider: 'strava', user: user }
    let(:activity) do
      FactoryGirl.create :activity, user: user, provider: r_identity.provider
    end

    it 'create activity in strava' do
      expect_any_instance_of(Providers::Strava).to receive(:create_activity).with(activity).and_return('1234')
      Jobs::Activity.perform 'id' => activity.id
    end
  end
end
