require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:user) { FactoryGirl.create :user }
  let(:identity) { FactoryGirl.create :identity, user: user }
  let(:activity) do
    FactoryGirl.create :activity, user: identity.user, provider: identity.provider
  end

  describe '#synched_with?' do
    it 'is in sync with strava' do
      activity.on_sync 'strava', '1234'
      activity.reload
      expect(activity.synched_with?('strava')).to be_truthy
      expect(activity.strava_uuid).to eq '1234'
    end
  end

  describe '#duplicate?' do
    it 'return duplication' do
      as = FactoryGirl.create :activity, user: identity.user, provider: 'runkeeper', uuid: 'abcd'
      as.on_sync 'strava', '1234'
      ad = FactoryGirl.build :activity, user: identity.user, provider: 'strava', uuid: '1234'
      expect(Activity.duplicate(ad).exists?).to be_truthy
    end
  end

  describe '#start_time_local' do
    it 'returns local time' do
      a = Activity.new start_time: DateTime.new(2019, 9, 12, 12, 12), utc_offset: 3
      expect(a.start_time_local).to eq 'Thu, 12 Sep 2019 15:12:00 +0300'
      a = Activity.new start_time: DateTime.new(2019, 9, 12, 12, 12), utc_offset: -3
      expect(a.start_time_local).to eq 'Thu, 12 Sep 2019 9:12:00 -0300'
    end
  end

  describe '#start_time_local=' do
    it 'transform local time to UTC time' do
      a = Activity.new

      a.start_time_local = { time: 'Thu, 12 Sep 2019 13:58:41', utc_offset: 3 }
      expect(a.start_time).to eq 'Thu, 12 Sep 2019 10:58:41.000000000 +0000'

      a.start_time_local = { time: 'Thu, 12 Sep 2019 13:58:41', utc_offset: nil }
      expect(a.start_time).to eq 'Thu, 12 Sep 2019 13:58:41.000000000 +0000'

      a.start_time_local = { time: 'Thu, 12 Sep 2019 13:58:41' }
      expect(a.start_time).to eq 'Thu, 12 Sep 2019 13:58:41.000000000 +0000'
    end
  end

  describe '#drift?' do
    let(:a) do
      FactoryGirl.build :activity, user: identity.user, provider: identity.provider
    end

    it 'checks type and start time' do
      activity
      expect(Activity.with_drift(a).exists?).to be_truthy
    end

    it 'allows 60 secs duration drift', focus: false do
      a.duration = activity.duration - 59
      expect(Activity.with_drift(a).exists?).to be_truthy
      a.duration = activity.duration + 59
      expect(Activity.with_drift(a).exists?).to be_truthy
    end

    it 'allows 100 meters distance drift' do
      a.distance = activity.distance - 99
      expect(Activity.with_drift(a).exists?).to be_truthy
      a.distance = activity.distance + 101
      expect(Activity.with_drift(a).exists?).to be_falsey
    end

    it 'allows 60 secs start time drift' do
      a.start_time = activity.start_time - 61.seconds
      expect(Activity.with_drift(a).exists?).to be_falsey
      a.start_time = activity.start_time + 59.seconds
      expect(Activity.with_drift(a).exists?).to be_truthy
    end
  end
end
