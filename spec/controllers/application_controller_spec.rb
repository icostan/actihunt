require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  before do
    user = FactoryGirl.create :user
    allow(controller).to receive(:current_user).and_return user
  end

  describe '#enqueue' do
    it 'enqueue job' do
      expect(Resque).to receive(:enqueue).with(Jobs::Activities, test: true)
      controller.enqueue Jobs::Activities, test: true
    end
    it 'create event' do
      expect do
        controller.enqueue Jobs::Activities, test: true
      end.to change(Event, :count).by 1
    end
  end
end
