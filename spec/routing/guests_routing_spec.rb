require 'rails_helper'

RSpec.describe GuestsController, type: :routing do
  describe 'routing' do
    it 'routes to #create' do
      expect(post: '/guests').to route_to('guests#create')
    end

    # it 'routes to #destroy' do
    #   expect(delete: '/guests/1').to route_to('guests#destroy', id: '1')
    # end
  end
end
