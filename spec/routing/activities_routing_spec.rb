require 'rails_helper'

RSpec.describe ActivitiesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/sources/s/activities').to route_to('activities#index', source_id: 's')
    end

    it 'routes to #show' do
      expect(get: '/sources/s/activities/1').to route_to('activities#show', id: '1', source_id: 's')
    end

    it 'routes to #destroy' do
      expect(delete: '/sources/s/activities/1').to route_to('activities#destroy', id: '1', source_id: 's')
    end
  end
end
