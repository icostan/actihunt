require 'rails_helper'

RSpec.describe 'activities/show', type: :view do
  before do
    allow(view).to receive(:current_source).and_return('strava')
    allow(view).to receive(:current_user).and_return(FactoryGirl.build(:user))
    @activity = assign(:activity, FactoryGirl.create(:activity, utc_offset: 2))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match('Show activity')
    expect(rendered).to match('Back')

    expect(rendered).to match('Source:')
    expect(rendered).to match(@activity.source)
    expect(rendered).to match('Uuid:')
    expect(rendered).to match(@activity.uuid)
    expect(rendered).to match('Type:')
    expect(rendered).to match(@activity.type)
    expect(rendered).to match('Duration:')
    # expect(rendered).to match('mins')
    expect(rendered).to match('Distance:')
    expect(rendered).to match("#{@activity.distance} m")
    expect(rendered).to match('Start time:')
    expect(rendered).to match('GMT')
    expect(rendered).to match('UTC offset:')
    expect(rendered).to match('2 hrs')
  end
end
