require 'rails_helper'

RSpec.describe 'activities/index', type: :view do
  let(:identity) { FactoryGirl.create :identity, provider: 'strava' }
  let(:a1) do
    FactoryGirl.create(:activity, user: identity.user, provider: identity.provider, uuid: 'abcd')
  end
  let(:a2) do
    FactoryGirl.create(:activity, user: identity.user, provider: identity.provider, distance: 1234)
  end

  before do
    allow(view).to receive(:current_user).and_return double(time_zone: 'Rome')
    allow(view).to receive(:current_source).and_return 'strava'
    assign(:activities, [a1, a2])
  end

  it 'renders a list of activities' do
    render
    expect(rendered).to match 'Listing Strava activities'
    expect(rendered).to match 'Synchronizing with'
    expect(rendered).to match 'Refresh'
    expect(rendered).to have_selector 'td.source', text: 'strava', count: 2
    expect(rendered).to have_selector 'td.uuid', count: 2
    expect(rendered).to have_selector 'td.type', count: 2
    expect(rendered).to have_selector 'td.duration', count: 2
    expect(rendered).to have_selector 'td.start_time', text: '+0', count: 2
    expect(rendered).to have_selector 'td.syncs', count: 2
  end
end
