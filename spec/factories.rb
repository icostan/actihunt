puts 'loading factories...'

FactoryGirl.define do
  factory :user do
    name Faker::Name.name
    email Faker::Internet.email
    time_zone 'Bucharest'
  end

  factory :identity do
    user
    provider Identity::PROVIDERS.sample
    uid SecureRandom.uuid
    access_token SecureRandom.uuid
  end

  factory :runkeeper, class: Identity do
    user
    provider 'runkeeper'
    uid 'rk'
    access_token '7a6064e63fb045ac97ce3266cef17b2f'
  end

  factory :strava, class: Identity do
    user
    provider 'strava'
    uid 'va'
    access_token '145c9aaa64f0329442de38037779572d5f89d8bb'
  end

  factory :activity do
    user
    provider Identity::PROVIDERS.sample
    uuid SecureRandom.uuid
    type Activity::TYPES.sample
    distance Kernel.rand(10_000)
    duration Kernel.rand(60) * 60
    start_time DateTime.now.utc
  end

  factory :point do
    type 'Gps'
    timestamp { rand(60) }
    lat { "47.1#{rand(1000)}" }
    lng { "27.5#{rand(1000)}" }
    distance { Kernel.rand(1000) }
    altitude { Kernel.rand(100) }
  end
end
