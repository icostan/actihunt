module ActihuntHelpers
  def lat
    "47.#{Kernel::rand(10000)}"
  end

  def lng
    "27.#{Kernel::rand(10000)}"
  end
end
