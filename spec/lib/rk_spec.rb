describe 'Rk::Client' do
  let(:access_token) { '7a6064e63fb045ac97ce3266cef17b2f' }
  subject { Rk::Client.new access_token }
  let(:id) { '972751976' }

  describe '#activity' do
    it 'loads activity' do
      activity = subject.activity id
      expect(activity.type).to eq 'Swimming'
      expect(activity.path.size).to eq 2

      point = activity.path.first
      expect(point.timestamp).to eq 0
      expect(point.altitude).to eq 186.5
      expect(point.latitude).to eq 47.998
      expect(point.longitude).to eq 27.4833
      expect(point.type).to eq 'start'
    end
  end
end
