require 'rails_helper'

describe Strava::Activity do
  let(:access_token) { '145c9aaa64f0329442de38037779572d5f89d8bb' }

  describe '.points', vcr: { record: :once } do
    it 'gets the points' do
      points = Strava::Activity.points double(uuid: '385182731'), access_token
      expect(points.size).to eq 433

      point = points.first
      expect(point[0]).to eq 0 # time
      expect(point[1]).to eq [47.189834, 27.523283] # latlng
      expect(point[2]).to eq 0.0 # distance
      expect(point[3]).to eq 69.2 # altitude

      point = points.last
      expect(point[0]).to eq 1919 # time
      expect(point[1]).to eq [47.195189, 27.554728] # latlng
      expect(point[2]).to eq 4912.4 # distance
      expect(point[3]).to eq 155.5 # altitude
    end

    it 'manual activities' do
      points = Strava::Activity.points double(uuid: '649693288'), access_token
      expect(points.size).to eq 0
    end
  end
end
