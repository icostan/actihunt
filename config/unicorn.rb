listen ENV['PORT'], backlog: ENV['UNICORN_BACKLOG'] && ENV['UNICORN_BACKLOG'].to_i || 16
worker_processes ENV['UNICORN_COUNT'] && ENV['UNICORN_COUNT'].to_i || 2
timeout ENV['UNICORN_TIMEOUT'] && ENV['UNICORN_TIMEOUT'].to_i || 30
preload_app true

before_fork do |_server, _worker|
end

after_fork do |_server, _worker|
end
