# frozen_string_literal: true

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :runkeeper, ENV['RUNKEEPER_KEY'], ENV['RUNKEEPER_SECRET']
  provider :strava, ENV['STRAVA_KEY'], ENV['STRAVA_SECRET'], scope: 'activity:read,activity:write', client_options: {
    site: 'https://www.strava.com/',
    authorize_url: 'https://www.strava.com/api/v3/oauth/authorize',
    token_url: 'https://www.strava.com/api/v3/oauth/token'
  }
end
OmniAuth.config.logger = Rails.logger if Rails.env.test?
