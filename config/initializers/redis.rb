def redis_url_from_env
  ENV['REDIS_URL']
end

def redis_url_from_docker
  if ENV['REDIS_PORT_6379_TCP_ADDR']
    "redis://#{ENV['REDIS_PORT_6379_TCP_ADDR']}:#{ENV['REDIS_PORT_6379_TCP_PORT']}"
  end
end

REDIS = Redis.new url: redis_url_from_env || redis_url_from_docker
