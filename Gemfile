# frozen_string_literal: true

source 'https://rails-assets.org'
source 'https://rubygems.org'

ruby '2.6.4'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'sprockets-rails', '~> 2.3'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Call 'byebug' anywhere in the code to stop execution and get a debugger console
gem 'byebug', group: %i[development test]

# Access an IRB console on exception pages or by using <%= console %> in views
gem 'web-console', group: :development

gem 'alchemist'
gem 'bootstrap-generators'
gem 'bootstrap3-datetimepicker-rails'
gem 'chronic_duration'
gem 'country_select'
gem 'haml-rails'
gem 'httparty'
gem 'less-rails'
gem 'less-rails-bootstrap'
gem 'momentjs-rails'
gem 'redis'
gem 'resque'
gem 'resque-web', require: 'resque_web'
gem 'runkeeper'
gem 'simple_form'
gem 'strava-ruby-client'
gem 'therubyracer'
# gem 'rails-assets-bootstrap-admin-template'
# gem 'rails-assets-bootstrap-theme-cirrus'
gem 'bower-rails'
gem 'bson_ext'
gem 'capybara', group: %i[development test]
gem 'capybara-screenshot', group: %i[development test]
gem 'cucumber-rails', require: false, group: %i[development test]
gem 'database_cleaner', group: %i[development test], git: 'https://github.com/DatabaseCleaner/database_cleaner.git'
gem 'devise'
gem 'dotenv-rails', groups: %i[development test]
gem 'facets', require: false
gem 'factory_girl_rails', group: %i[development test]
gem 'faker', group: %i[development test]
gem 'foreman'
gem 'geckodriver-helper'
gem 'gpx'
gem 'guard', group: %i[development test]
gem 'guard-cucumber', group: %i[development test]
gem 'guard-jasmine', group: %i[development test]
gem 'guard-rspec', group: %i[development test]
gem 'guard-zeus', group: %i[development test]
gem 'hashie'
gem 'intercom-rails'
gem 'jasmine-rails', group: %i[development test]
gem 'mongoid'
gem 'mongoid-tree', group: %i[development test]
gem 'newrelic_rpm'
gem 'omniauth'
gem 'omniauth-oauth2', '1.3.1' # oauth2 flow fails with 1.4.1
gem 'omniauth-runkeeper'
gem 'omniauth-strava'
gem 'powder', group: [:development]
gem 'pry'
gem 'pry-doc'
gem 'rack-tracker'
gem 'rspec-rails', group: %i[development test]
gem 'rubocop'
gem 'selenium-webdriver', group: %i[development test]
gem 'vcr', group: [:test]
gem 'webmock', group: [:test]
gem 'zeus', group: [:development]
gem 'poltergeist'
gem 'irb', group: [:development]
gem 'reek', group: [:development]
gem 'solargraph', group: [:development]
gem 'json'

gem 'bugsnag', '~> 6.7'
