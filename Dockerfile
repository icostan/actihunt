FROM ruby:2.3-onbuild

RUN rake assets:precompile

ENV RAILS_ENV production

CMD ["rails", "console"]