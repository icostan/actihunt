class IdentitiesController < ApplicationController
  def destroy
    current_identity.destroy
    redirect_to user_path(:current)
  end

  protected

  def current_identity
    current_user.identity params[:id]
  end
end
