class ActivitiesController < ApplicationController
  before_filter :current_source
  before_action :collection, only: [:index, :create]
  before_action :current_activity, only: [:sync, :show, :destroy]

  helper_method :current_source

  def create
    options = Jobs::Activities.payload current_identity
    enqueue Jobs::Activities, options

    redirect_to source_activities_path current_source
  end

  def index
  end

  def show
  end

  def destroy
    current_activity.destroy

    redirect_to source_activities_path current_source
  end

  def sync
    payload = Jobs::Activity.payload current_activity
    # enqueue Jobs::Activity, payload
    Jobs::Activity.do_perform payload.stringify_keys

    redirect_to source_activities_path current_source
  end

  private

  def current_source
    @source ||= params[:source_id]
  end

  def current_identity
    @identity ||= current_user.identity current_source
  end

  def collection
    @activities ||= current_user.activities.where(provider: current_source).order_by(:start_time.desc).limit 50
  end

  def current_activity
    @activity = current_user.activities.find(params[:id])
  end

  def activity_params
    params.require(:activity).permit(:duration, :distance)
  end
end
