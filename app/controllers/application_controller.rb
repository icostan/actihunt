class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :authenticate_user!, except: [:home, :contact]

  def home
    render :home
  end

  def contact
    render :contact
  end

  def after_sign_in_path_for(resource)
    source_activities_path resource.identities.first.provider
  end

  def enqueue(job, options)
    Jobs.enqueue job, options, current_user
  end
end
