module Users
  class RegistrationsController < Devise::RegistrationsController
    before_filter :configure_permitted_parameters

    protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit :account_update, keys: [:name, :email, :time_zone]
    end

    def update_resource(resource, params)
      resource.update_attributes params
    end

    def after_update_path_for(_resource)
      edit_user_path
    end
  end
end
