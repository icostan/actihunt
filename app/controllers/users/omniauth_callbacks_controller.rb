module Users
  class OmniauthCallbacksController < Devise::OmniauthCallbacksController
    def self.provides_callback_for(provider)
      class_eval %{
        def #{provider}
          @user = User.from_omniauth omniauth, current_user
          if @user.persisted?
            sign_in_and_redirect @user, event: :authentication
            if is_navigational_format?
              set_flash_message :notice, :success, kind: "#{provider}"
            end
          else
            session["devise.#{provider}_data"] = omniauth
            redirect_to new_user_registration_url
          end
        end
      }
    end

    Identity::PROVIDERS.each do |provider|
      provides_callback_for provider
    end

    private

    def omniauth
      request.env['omniauth.auth']
    end
  end
end
