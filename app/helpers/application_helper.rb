module ApplicationHelper
  def identity_class(identity)
    params[:source_id].to_s == identity.provider.to_s ? 'active' : ''
  end
end
