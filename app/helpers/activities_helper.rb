module ActivitiesHelper
  def distance(activity)
    "#{activity.distance} m"
  end

  def duration(activity)
    ChronicDuration.output(activity.duration)
  end

  def start_time(activity)
    tz = current_user.time_zone || 'UTC'
    t = activity.start_time.in_time_zone tz
    t.to_s
  end
end
