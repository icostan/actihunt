class Identity
  include Mongoid::Document
  include Mongoid::Timestamps

  RUNKEEPER = 'runkeeper'.freeze
  STRAVA = 'strava'.freeze
  PROVIDERS = [RUNKEEPER, STRAVA].freeze

  belongs_to :user
  field :access_token, type: String
  field :provider, type: String
  field :uid, type: String
  has_many :activities

  validates :provider, :uid, :access_token, presence: true

  def self.from_omniauth(auth)
    provider = auth.provider
    uid = auth.uid

    where(provider: provider, uid: uid).first_or_create do |identity|
      identity.access_token = auth.credentials.token
    end
  end

  def link(user)
    update_attributes user: user
  end
end
