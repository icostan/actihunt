class Event
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user
  field :name, type: String
  field :job, type: String
  field :options, type: Hash
end
