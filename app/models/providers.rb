module Providers
  def self.provider(identity)
    provider_class = "Providers::#{identity.provider.titleize}"
    provider_class.constantize.new identity.access_token
  end

  def self.for_sync(activity)
    identities = activity.user.identities.reject do |identity|
      identity.provider == activity.provider || activity.synched_with?(identity.provider)
    end
    identities.map do |identity|
      provider identity
    end
  end
end
