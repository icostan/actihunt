class User
  include Mongoid::Document
  include Mongoid::Timestamps

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, omniauth_providers: [:runkeeper, :strava]

  ## Database authenticatable
  # field :email,              type: String, default: ''
  # field :encrypted_password, type: String, default: ''

  ## Recoverable
  # field :reset_password_token,   type: String
  # field :reset_password_sent_at, type: Time

  ## Rememberable
  # field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  field :name
  field :email
  field :time_zone
  field :admin
  has_many :identities
  has_many :activities

  validates :email, uniqueness: true,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+.)+[a-z]{2,})\z/i },
            if: :email?

  def self.from_omniauth(auth, signed_in_user)
    identity = Identity.from_omniauth auth
    if signed_in_user
      user = signed_in_user
    else
      user = identity.user
      user ||= User.create!(name: auth.info.name, email: auth.info.email)
    end
    identity.link user unless identity.user
    user
  end

  def connected_with?(provider)
    identities.where(provider: provider).exists?
  end

  def identity(name)
    identities.where(provider: name).first
  end

  def provider(name)
    Providers.provider identity(name)
  end
end
