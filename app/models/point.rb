# TODO: rename point model to waypoint
class Point
  include Mongoid::Document
  include Mongoid::Timestamps

  TYPES = %w[Start Gps Pause Resume Manual End].freeze

  embedded_in :activity

  # TODO: remove, not needed, it RK specific
  field :type, type: String
  field :timestamp, type: Integer # the time offset, first point is 0
  field :lat, type: Float
  field :lng, type: Float
  field :distance, type: Float
  field :altitude, type: Float

  validates_inclusion_of :type, in: TYPES
end
