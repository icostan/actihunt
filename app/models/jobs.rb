module Jobs
  def self.enqueue(job, options, user = nil)
    Resque.enqueue job, options
    Event.create! user: user, job: job.name, options: options
  end

  class Base
    def logger
      Jobs::Base.logger
    end

    def self.perform(options)
      log "Executing #{options}..."
      do_perform options
      log 'Done.'
    end

    def self.logger
      Rails.logger
    end

    def self.log(message)
      puts "==> #{name}: #{message}"
    end
  end

  class Activities < Base
    @queue = :activities

    def self.payload(identity)
      {
        id: identity.id.to_s,
        user_name: identity.user.name
      }
    end

    def self.do_perform(options)
      identity = Identity.find options['id']
      provider = Providers.provider identity
      log "Pulling activities from #{provider.name}..."
      provider.pull_activities do |activity|
        title = "#{activity.type}##{activity.uuid}"
        if ::Activity.duplicate(activity).exists?
          log "Skipping duplicate: #{title}"
        elsif ::Activity.with_drift(activity).exists?
          log "Skipping drift: #{title}"
        elsif activity.api?
          log "Skipping API: #{title}"
        else
          activity.identity = identity
          activity.user = identity.user
          activity.provider = identity.provider
          if activity.save
            log "Saved and syncing activity: #{title}"
            payload = Jobs::Activity.payload activity
            Jobs.enqueue Jobs::Activity, payload, identity.user
          else
            log "Saving FAILED: #{title} - #{activity.errors.full_messages}"
          end
        end
      end
    end
  end

  class Activity < Base
    @queue = :activities

    def self.payload(activity)
      {
        id: activity.id.to_s,
        user_name: activity.user&.name
      }
    end

    def self.do_perform(options)
      activity = ::Activity.find options['id']
      providers = Providers.for_sync activity
      providers.each do |provider|
        uuid = provider.create_activity activity
        activity.on_sync provider.name, uuid
        log "Sync with #{provider.name} - ##{uuid} done."
      end
    end
  end
end
