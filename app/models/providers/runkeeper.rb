module Providers
  class Runkeeper
    TYPES = %w[Running Cycling Walking Swimming Hiking Snowboarding Skating Stairmaster\ /\ Stepwell Elliptical Downhill\ Skiing Rowing Spinning Rowing].freeze

    attr_reader :client

    def initialize(access_token)
      @runkeeper = ::Runkeeper.new access_token
      @client = ::Rk::Client.new access_token
    end

    def name
      Identity::RUNKEEPER
    end

    def activities
      @runkeeper.fitness_activities['items']
    end

    def pull_activities
      activities.each do |a|
        yield build_activity a
      end
    end

    def create_activity(activity)
      options = {
        type: to_type(activity),
        duration: activity.duration,
        total_distance: activity.distance,
        start_time: activity.start_time_local,
        utc_offset: activity.utc_offset
      }
      path = path activity
      options[:path] = path unless path.empty?

      rk_activity = @runkeeper.new_activity options
      response = rk_activity.save
      raise "runkeeper activity failed: #{response.body}" unless response.success?
      rk_activity.id
    end

    def uuid(ra_or_hash)
      if ra_or_hash.respond_to? :id
        ra_or_hash.id
      elsif ra_or_hash['uri'] =~ /(\d+)/
        Regexp.last_match 1
      else
        raise "runkeeper activity # not found #{ra_or_hash.inspect}"
      end
    end

    def path(activity)
      activity.points.map do |point|
        next if point.lat.blank? || point.lng.blank?
        {
          latitude: point.lat,
          longitude: point.lng,
          altitude: point.altitude || 0,
          timestamp: point.timestamp,
          type: point.type.downcase
        }
      end.compact
    end

    def to_type(activity)
      index = Activity::TYPES.index(activity.type)

      raise "Activity #{activity.type} not supported in Runkeeper" unless index
      TYPES[index]
    end

    def from_type(r_activity)
      index = TYPES.index(r_activity['type'])
      raise "Runkeeper activity #{r_activity['type']} not supported" unless index

      Activity::TYPES[index]
    end

    private

    def build_activity(a)
      Activity.new do |activity|
        id = uuid a
        activity.uuid = id
        activity.type = from_type(a)
        activity.start_time_local = { time: a['start_time'], utc_offset: a['utc_offset'] }
        activity.utc_offset = a['utc_offset'] if a['utc_offset']
        activity.duration = a['duration']
        activity.distance = a['total_distance']
        # activity.api = a['entry_mode'] == 'API'
        activity.points = build_points(id)
      end
    end

    def build_points(id)
      activity = client.activity(id)
      activity.path.map do |p|
        ::Point.new(type: p.type.titleize, timestamp: p.timestamp,
                    lat: p.latitude, lng: p.longitude,
                    altitude: p.altitude)
      end
    end
  end
end
