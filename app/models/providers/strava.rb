require 'gpx'
require 'facets/enumerable'

module Providers
  class Strava
    include GPX

    TYPES = %w[Run Ride Walk Swim Hike Snowboard IceSkate StairStepper Elliptical AlpineSki StandUpPaddling EBikeRide Kayaking].freeze

    def initialize(access_token)
      @access_token = access_token
      @strava = ::Strava::Api::Client.new access_token: access_token
    end

    def name
      Identity::STRAVA
    end

    def activities
      @strava.athlete_activities
    end

    def pull_activities
      activities.each do |a|
        yield pull_activity a
      end
    end

    def pull_activity(a)
      s_activity = Activity.new do |activity|
        activity.uuid = uuid(a)
        activity.type = from_type(a['type'])
        activity.start_time = a['start_date']
        activity.utc_offset = a['utc_offset'] / 60 / 60 if a['utc_offset']
        activity.duration = a['elapsed_time']
        activity.distance = a['distance']
        activity.points = build_waypoints activity
      end
      s_activity
    end

    def build_waypoints(activity)
      points = ::Strava::Activity.points activity, @access_token
      points.map_with_index do |point, index|
        type = case index
               when 0
                 ::Point::TYPES.first
               when points.size - 1
                 ::Point::TYPES.last
               else
                 ::Point::TYPES[1]
               end
        waypoint = ::Point.new(type: type, timestamp: point[0],
                               distance: point[2], altitude: point[3])
        waypoint.lat = point[1].first if point[1]
        waypoint.lng = point[1].last if point[1]
        waypoint
      end
    end

    def create_activity(activity)
      if activity.points.empty?
        post_activity activity
      else
        upload_activity activity
      end
    end

    def uuid(a)
      a['id']
    end

    def to_type(activity_type)
      index = Activity::TYPES.index(activity_type)
      raise "Activity #{activity_type} not supported Strava" unless index

      TYPES[index]
    end

    def from_type(strava_type)
      index = TYPES.index strava_type
      raise "Strava activity #{strava_type} not supported" unless index

      Activity::TYPES[index]
    end

    private

    def post_activity(activity)
      type = to_type activity.type
      distance = activity.distance > 0 ? activity.distance : 1
      s_activity = ::Strava::Activity.create(type, distance,
                                             activity.duration,
                                             activity.start_time,
                                             @access_token)
      s_activity['id']
    end

    def upload_activity(activity)
      data = gpx_data activity
      response = @strava.create_upload data
      log "Strava create: #{response}"
      while uploading? response
        sleep 3
        response = @strava.upload(response['id'])
        raise RuntimeError, response.inspect if response['error']
      end
      response['activity_id']
    rescue ::Strava::Errors::Fault => e
      puts e.inspect
      raise e
    end

    def gpx_data(activity)
      options = {}
      options[:name] = to_type(activity.type)
      options[:activity_type] = to_type(activity.type)
      options[:data_type] = 'gpx'
      options[:file] = gpx_export activity
      options[:external_id] = activity.id
      options
    end

    def gpx_export(activity)
      gpx_file = GPXFile.new
      gpx_file.name = activity.type
      gpx_file.time = activity.start_time
      track = Track.new
      track.name = activity.type
      segment = Segment.new
      activity.points.each do |point|
        options = {
          lat: point.lat, lon: point.lng,
          time: gpx_timestamp(activity, point), elevation: point.altitude
        }
        segment.points << TrackPoint.new(options)
      end
      track.segments << segment
      gpx_file.tracks << track

      filename = gpx_filename activity
      log("GPX: #{gpx_file}")
      gpx_file.write filename
      # File.new filename
      Faraday::UploadIO.new filename, 'application/gpx+xml'
    end

    def gpx_timestamp(activity, point)
      Time.at(activity.start_time.to_i + point.timestamp)
    end

    def gpx_filename(activity)
      File.join(Dir.tmpdir, "strava-#{activity.uuid}.gpx")
    end

    def uploading?(response)
      response['status'].include? 'being processed'
    end

    def log(msg)
      Rails.logger.debug "Strava: #{msg}"
    end
  end
end
