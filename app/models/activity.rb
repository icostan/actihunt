class Activity
  include Mongoid::Document
  include Mongoid::Timestamps

  TYPES = %w[Running Cycling Walking Swimming Hiking Snowboarding Skating Stairmaster Elliptical Skiing Paddling Spinning Kayaking].freeze

  belongs_to :user
  field :provider
  belongs_to :identity

  field :uuid, type: String
  field :strava_uuid, type: String
  field :runkeeper_uuid, type: String
  field :type, type: String
  field :api, type: Boolean
  field :duration, type: Integer
  field :distance, type: Integer
  field :start_time, type: DateTime
  field :utc_offset, type: Integer, default: 0
  field :climb, type: Integer
  field :syncs, type: Hash, default: {}

  embeds_many :points

  validates :user, :uuid, :provider, presence: true
  validates :type, :duration, :distance, :start_time, presence: true
  validates :provider, inclusion: { in: Identity::PROVIDERS }

  validates_uniqueness_of :uuid, scope: :user
  validates_uniqueness_of :user, scope: %i[type distance duration start_time]

  scope :duplicate, (lambda { |a|
                        if a.provider == 'runkeeper'
                          where(provider: 'strava', runkeeper_uuid: a.uuid)
                        else
                          where(provider: 'runkeeper', strava_uuid: a.uuid)
                        end
                      })

  scope :with_drift, (lambda { |a|
                        t = type?(a)
                        t = t.duration_with_drift(a)
                        t = t.distance_with_drift(a)
                        t = t.start_time_with_drift(a)
                        t
                      })
  scope :type?, (->(a) { where(type: a.type) })
  scope :duration_with_drift, (lambda { |a|
                                 drift = 60 # seconds
                                 where(:duration.gt => a.duration - drift,
                                       :duration.lt => a.duration + drift)
                               })
  scope :distance_with_drift, (lambda { |a|
                                 drift = 100 # meters
                                 where(:distance.gt => a.distance - drift,
                                       :distance.lt => a.distance + drift)
                               })
  scope :start_time_with_drift, (lambda { |a|
                                   drift = 60.seconds
                                   where(:start_time.gt => a.start_time - drift,
                                         :start_time.lt => a.start_time + drift)
                                 })

  def source
    provider
  end

  def synched_with?(source)
    source_uuid = send "#{source}_uuid"
    !source_uuid.blank?
  end

  def on_sync(source, uuid)
    self.syncs[source.to_s] = uuid
    self.send "#{source}_uuid=", uuid
    save
  end

  def start_time_local
    start_time.new_offset "#{utc_offset>0 ? '+' : '-'}#{utc_offset}"
  end

  def start_time_local=(time:, utc_offset: 0)
    utc_offset ||= 0
    self.start_time = DateTime.parse(time)
    self.start_time -= Rational(utc_offset, 24)
  end
end
