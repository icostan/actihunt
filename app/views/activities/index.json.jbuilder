json.array!(@activities) do |activity|
  json.extract! activity, :id, :source, :uuid, :type, :duration
  json.url activity_url(activity, format: :json)
end
