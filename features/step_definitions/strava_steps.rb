Given(/^a sign in strava account$/) do
  step 'I sign in with "strava"'
  step 'strava authenticate with "test@actihunt.com", "hunter15"'
  @access_token = Identity.where(provider: 'strava').first.access_token
  @strava = Providers::Strava.new @access_token
end

Given(/^a strava account$/) do
  @access_token = '7a8f01f37c941389051703f4d608d5e184edf0be'
  @strava = Providers::Strava.new @access_token
end

When(/^I create strava activity$/) do
  activity = FactoryGirl.create :activity, type: Activity::TYPES.sample, distance: 0
  @success = @strava.create_activity activity
end

When(/^I upload strava activity$/) do
  @now = DateTime.now.utc
  @activity = FactoryGirl.create :activity, start_time: @now, utc_offset: 3
  # puts "UAT: #{@activity.inspect}"
  (1..5).each do |_i|
    @activity.points << FactoryGirl.build(:point)
  end

  @id = @strava.create_activity @activity
end

Then(/^I have strava activity created$/) do
  expect(@success).to be_truthy
end

Then(/^I have strava activity uploaded$/) do
  expect(@id).to be_truthy

  activities = @strava.activities
  # puts "Activities: #{activities}"
  activity = activities.detect{ |a| a.id == @id }
  # puts "Activity: #{activity.inspect}"
  expect(activity['start_date'].to_s).to include @now.strftime('%Y-%m-%d %H:%M')
  expect(activity['utc_offset']).to eq 10800
end
