Given(/^no guests$/) do
  Guest.delete_all
end

Given(/^I am in landing page$/) do
  visit '/'
end

When(/^I register email$/) do
  fill_in :email, with: 'test@actihunt.com'
  click_button 'Subscribe'
end

Then(/^I should guest created$/) do
  expect(Guest.count).to eq 1
  expect(Guest.first.email).to eq 'test@actihunt.com'
end
