Given(/^an "(.*?)" activity$/) do |source|
  user = User.last
  identity = user.identity source
  @activity = FactoryGirl.create :activity, identity: identity, user: user, provider: identity.provider
end

Given(/^"(.*?)" activities page$/) do |source|
  visit "/sources/#{source}/activities"
end

When(/^run job$/) do
  sleep 1
  event = Event.first
  event.job.constantize.perform event.options
end

When(/^I refresh activities$/) do
  click_link 'Refresh'
  step 'run job'
end

When(/^I sync activity$/) do
  step 'refresh page'
  click_link 'Sync'
  step 'run job'
end

Then(/^I have list of runkeeper activities$/) do
  activity = User.first.identities.first.activities.first
  step 'refresh page'
  expect(page).to have_content activity.uuid
  expect(page).to have_content activity.type
  expect(page).to have_content ChronicDuration.output(activity.duration)
  expect(page).to have_content "#{activity.distance} m"
end

Then(/^I have list of strava activities$/) do
  activity = User.first.identities.first.activities.first
  step 'refresh page'
  expect(page).to have_content activity.uuid
  expect(page).to have_content activity.type
  expect(page).to have_content ChronicDuration.output(activity.duration)
  expect(page).to have_content "#{activity.distance} m"
end

Then(/^refresh page$/) do
  visit current_url
end

Then(/^It should be synched with "(.*?)"$/) do |source|
  @activity.reload
  expect(@activity.synched_with?(source)).to be_truthy
end
