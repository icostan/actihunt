Given(/^Landing page$/) do
  visit '/'
end

Given(/^I sign in with "(.*?)"$/) do |provider|
  click_link provider
end

When(/^strava authenticate with "(.*?)", "(.*?)"$/) do |email, password|
  if has_field? :email
    fill_in :email, with: email
    fill_in :password, with: password
    click_button 'Log In'
  end
  click_button 'Allow' if has_button? 'Allow'
  click_button 'Authorize' if has_button? 'Authorize'
end

When(/^runkeeper authenticate with "(.*?)", "(.*?)"$/) do |email, password|
  asics_window = window_opened_by do
    click_link 'Log In'
  end
  within_window asics_window do
    fill_in :a_email, with: email
    fill_in :a_password, with: password
    click_button 'Log In'
  end
end

Then(/^I am signed in as "(.*?)", "(.*?)"$/) do |name, uid|
  expect(page).to have_content(name)
end

Then(/I land in "(.*?)"/) do |title|
  expect(page).to have_content(title)
end

Then(/^I have one user with "(.*?)" accounts$/) do |count|
  expect(User.count).to eq 1
  expect(User.first.identities.count).to eq count.to_i
end
