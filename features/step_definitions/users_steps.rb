When(/^Edit user account$/) do
  click_link 'account'
end

When(/^Update user account$/) do
  fill_in :user_name, with: 'Actihunt'
  fill_in :user_email, with: 'test@actihunt.com'
  click_button 'Update'
end

Then(/^Account is updated$/) do
  expect(page).to have_content 'Your account has been updated successfully.'
  expect(page).to have_selector("input[value='Actihunt']")
  expect(page).to have_selector("input[value='test@actihunt.com']")
end
