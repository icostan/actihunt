Given(/^a sign in runkeeper account$/) do
  step 'I sign in with "runkeeper"'
  step 'runkeeper authenticate with "test@actihunt.com", "wks-bY2-EWk-Qxv"'
  sleep 5
  access_token = Identity.where(provider: 'runkeeper').first.access_token
  @runkeeper = Providers::Runkeeper.new access_token
end

Given(/^a runkeeper account$/) do
  access_token = '7a6064e63fb045ac97ce3266cef17b2f'
  @runkeeper = Providers::Runkeeper.new access_token
end

When(/^I create runkeeper activity$/) do
  @activity = FactoryGirl.create :activity, start_time: DateTime.new(2019, 9, 3, 19, 26), utc_offset: 3
  # puts "UAT: #{@activity.inspect}"
  (1..5).each do |_i|
    @activity.points << FactoryGirl.build(:point)
  end

  @id = @runkeeper.create_activity @activity
end

Then(/^I have runkeeper activity created$/) do
  expect(@id).to be_truthy

  activities = @runkeeper.activities
  activity = activities.detect{ |a| a['uri'] =~ /#{@id}/}
  # puts "Activity: #{activity.inspect}"
  expect(activity['start_time']).to eq 'Tue, 3 Sep 2019 22:26:00'
  expect(activity['utc_offset']).to eq 3
end
