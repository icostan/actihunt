Feature: Activities

  Background:
    Given Landing page

  # @javascript
  # Scenario: Runkeeper activities index
  #   Given a sign in runkeeper account
  #   And "runkeeper" activities page
  #   When I refresh activities
  #   Then I have list of runkeeper activities

  @javascript
  Scenario: Strava activities index
    Given a sign in strava account
    And "strava" activities page
    When I refresh activities
    Then I have list of strava activities

  # @javascript
  # Scenario: Sync activity
  #   Given a sign in runkeeper account
  #   And Landing page
  #   And a sign in strava account
  #   And "runkeeper" activities page
  #   And an "runkeeper" activity
  #   When I sync activity
  #   Then It should be synched with "strava"
