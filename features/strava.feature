Feature: Strava

  Background:
    Given Landing page

  @javascript
  Scenario: Post activity
    Given a sign in strava account
    When I create strava activity
    Then I have strava activity created

  @javascript
  Scenario: Upload activity
    Given a sign in strava account
    When I upload strava activity
    Then I have strava activity uploaded
