Feature: Login

  Background:
    Given Landing page

    # @javascript
    # Scenario: Login with Runkeeper
    #   Given I sign in with "runkeeper"
    #   When I authenticate with "test@actihunt.com", "wks-bY2-EWk-Qxv"
    #   Then I am signed in as "Test Hunt", "43063004"
    #   And I land in "Listing Runkeeper activities"

  @javascript
  Scenario: Login with Strava
    Given a sign in strava account
    When strava authenticate with "test@actihunt.com", "hunter15"
    Then I am signed in as "Test Actihunt", "9015828"
    And I land in "Listing Strava activities"

  # @javascript
  # Scenario: Connect with Runkeeper and Strava
  #   Given I sign in with "runkeeper"
  #   When runkeeper authenticate with "test@actihunt.com", "wks-bY2-EWk-Qxv"
  #   Given Landing page
  #   Given I sign in with "strava"
  #   When strava authenticate with "test@actihunt.com", "hunter15"
  #   Then I have one user with "2" accounts
