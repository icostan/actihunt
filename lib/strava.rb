# TODO: create Strava::Client
module Strava
  API_URL = 'https://www.strava.com/api/v3/activities'.freeze

  class Activity
    def self.create(type, distance, duration, start_time, access_token)
      body = {
        name: type,
        elapsed_time: duration, distance: distance,
        start_date_local: formatted_time(start_time), type: type
      }

      response = HTTParty.post API_URL, headers: headers(access_token), query: body
      response
    end

    def self.points(activity, access_token)
      response = HTTParty.get "#{API_URL}/#{activity.uuid}/streams/time,altitude,latlng,distance",
                              headers: headers(access_token), query: { resolution: :medium }
      time = stream response, 'time'
      latlng = stream response, 'latlng'
      distance = stream response, 'distance'
      altitude = stream response, 'altitude'

      time.zip latlng, distance, altitude
    end

    def self.headers(access_token)
      { 'Authorization' => "Bearer #{access_token}" }
    end

    def self.stream(response, type)
      series = response.select do |stream|
        stream['type'] == type
      end
      series.empty? ? [] : series.first['data']
    end

    def self.formatted_time(time)
      time.strftime '%Y-%m-%dT%H:%M:%SZ'
    end
  end
end
