desc 'Poll latest activities'
task poll: :environment do
  puts '==> Poll: start'
  Identity.all.each do |identity|
    puts "==> Poll: update #{identity.user.name} - #{identity.provider}..."
    job = Jobs::Activities
    options = job.payload identity
    Resque.enqueue job, options
  end
  puts '==> Poll: done'
end
