namespace :migration do
  desc 'use strava and runkeeper uuid instead of syncs'
  task syncs_to_uuids: :environment do
    count = Activity.where(strava_uuid: nil, runkeeper_uuid: nil).count
    puts "==> Synching #{count} activities..."
    Activity.all.each do |activity|
      if activity.strava_uuid.blank? && activity.runkeeper_uuid.blank?
        puts "==> Synching #{activity.type} activity"
        activity.strava_uuid = activity.syncs['strava']
        activity.runkeeper_uuid = activity.syncs['runkeeper']
        activity.save
      end
    end
  end

  desc 'link activities to user'
  task link_activity_to_user: :environment do
    Activity.all.each do |activity|
      if activity.identity.nil?
        puts "==! Orphan activity #{activity.type}"
        next
      else
        user = activity.identity.user
        puts "==> Synching #{activity.type} activity for #{user.name}"
        activity.user = user
        activity.provider = activity.identity.provider
        activity.save
      end
    end
  end
end
