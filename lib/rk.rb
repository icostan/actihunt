module Rk
  class Client
    include HTTParty
    base_uri 'https://api.runkeeper.com'
    format :json

    def initialize(access_token)
      @access_token = access_token
    end

    def activity(id)
      response = get "/fitnessActivities/#{id}"
      Hashie::Mash.new response
    end

    private

    def get(uri)
      response = self.class.get uri, headers: headers
      raise "Runkeeper: get '#{uri}' failed: #{response.message}" unless response.success?
      response
    end

    def headers
      {
        'Authorization' => "Bearer #{@access_token}",
        'Accept' => 'application/vnd.com.runkeeper.FitnessActivity+json'
      }
    end
  end
end

class Runkeeper
  class Activity
    attr_accessor :type, :start_time, :total_distance, :duration, :average_heart_rate, :total_calories, :notes, :path, :post_to_facebook, :post_to_twitter, :utc_offset

    def to_hash
      main = {
        :type => type,
        :start_time => formatted_start_time,
        :duration => duration,
        :utc_offset => utc_offset
      }
      main.merge! optional_attrs
    end
  end
end
